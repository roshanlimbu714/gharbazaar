import React, {useEffect, useState} from "react";
import {Link, useHistory, useParams} from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from '../../store/actions'
import mockLand from "../../assets/images/mock.png";

const Search = (props) => {
    const history = useHistory();
    const {term} = useParams();
    const [search, setSearch] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    useEffect(() => {
        props.searchLand(term);
    }, [term]);

    useEffect(() => {
        setSearchResults(props.searchResults);
        console.log(searchResults);
    }, [props.searchResults]);

    return <div className={'search-page'}>
        <div className="title">{} results found from {term}</div>
        <div className="input-area d-flex align-center">
            <input
                type="text"
                name="SearchTitle"
                onChange={e => setSearch(e.target.value)}
                className="flex-one"
                placeholder="Search Everything"
            />
            <i className="material-icons"
               onClick={() => {
                   history.push("/search/" + search);
               }}>
                search
            </i>
        </div>
        <div className="search-results d-flex flex-wrap my-5">
            {props.searchResults[0] && props.searchResults.map(land => (
                <article>
                    <div className="article-img">
                        <img src={mockLand} alt={'mock'}/>
                    </div>
                    <Link to={'/land/'+ land._id} className="article-title">{land.title}</Link>
                    <div className="article-description">
                        <i className="material-icons">room</i>
                        {land.address}
                    </div>
                    <div className="article-description d-flex align-center primary-text">
                        <b className="">Rs. </b>
                        <div className="flex-one">Price</div>
                    </div>
                    <div className="article-subtitle d-flex align-center">
                        <i className="material-icons">face</i>Ram Bilas
                    </div>
                </article>
            ))}
        </div>
    </div>;
};

const mapStateToProps = state => {
    return {
        searchResults: state.landReducer.searchResults
    }
};
const mapDispatchToProps = dispatch => {
    return {
        searchLand: (term) => dispatch(actions.searchLand(term))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
