import React, {useState} from 'react';
import {connect} from "react-redux";
import * as action from '../../store/actions/index';

const Login = (props) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    let loginUser = (e) => {
        e.preventDefault();
        props.onAuth(username, password);
    };
    return (
        <section className="login-section flex-centered">
            <div className="background" onClick={props.displayLogin}></div>
            <form className="card d-flex flex-column" onSubmit={(e) => loginUser(e)}>
                <div className="title">Login</div>
                <div className="input-area flex-one flex-centered flex-column">
                    <div className="input-box icon-input">
                        <input type="text" placeholder={'Enter username'} onChange={(e) => setUsername(e.target.value)}/>
                        <i className="material-icons">face</i>
                    </div>
                    <div className="input-box icon-input">
                        <input type="password" placeholder={'Password'} onChange={(e) => setPassword(e.target.value)}/>
                        <i className="material-icons">lock</i>
                    </div>
                </div>
                <button className={'btn primary'}>Login</button>
            </form>
        </section>
    );
};


const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(action.authenticate(username, password)),
        displayLogin: () => dispatch(action.displayLoginModal(false))
    }
};

const mapStateToProps = state => {
    return {
        user: state.authenticationReducer.user
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
