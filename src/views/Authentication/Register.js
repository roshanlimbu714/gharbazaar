import React, {useState} from 'react';
import image from '../../assets/images/background.jpeg'
import {connect} from "react-redux";
import * as actions from '../../store/actions/index';

const Register = (props) => {
    const [name, setName] = useState('')
    const [username, setUsername] = useState('')
    const [address, setAddress] = useState('')
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('')

    let register = (e) => {
        e.preventDefault();
        props.registerUser({name, username, address, phone, password});
    };
    return (
        <section className={'register wrapper d-flex'}>
            <div className="image-area">
                <img src={image} alt=""/>
            </div>
            <div className="text-area">
                <form onSubmit={e => register(e)}>
                    <div className="input-box">
                        <label>Name</label>
                        <input type="text" placeholder={'Enter name'} onKeyUp={(e) => setName(e.target.value)}/>
                    </div>
                    <div className="input-box">
                        <label>Username</label>
                        <input type="text" placeholder={'Enter username'} onKeyUp={(e) => setUsername(e.target.value)}/>
                    </div>
                    <div className="input-box">
                        <label>Password</label>
                        <input type="text" placeholder={'Enter password'} onKeyUp={(e) => setPassword(e.target.value)}/>
                    </div>
                    <div className="input-box">
                        <label>Address</label>
                        <input type="text" placeholder={'Enter address'} onKeyUp={(e) => setAddress(e.target.value)}/>
                    </div>
                    <div className="input-box">
                        <label>Phone</label>
                        <input type="tel" placeholder={'Enter phone'} onKeyUp={(e) => setPhone(e.target.value)}/>
                    </div>
                    <button className={'btn primary'}>Register</button>
                </form>
            </div>
        </section>
    );
};

const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = (dispatch) => {
    return {
        registerUser: (user) => dispatch(actions.registerUser(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
