import React, {useState} from "react";
import {Redirect, Route, Switch} from "react-router";
import Messages from "./containers/Messages";
import AddLand from "./containers/AddLand";
import Lands from "./containers/Lands";
import {connect} from "react-redux";
import {toggleAddLand} from "../../store/actions";
import {NavLink} from "react-router-dom";
import Profile from "./containers/Profile";
import Land from "./containers/Land";

const Merchant = props => {
    const [isAddLandModalOpen, setIsAddLandModalOpen] = useState(false);
    return (
        <>
            <section className={"merchant-section"}>
                <div className="page-top d-flex justify-between">
                    <div className="page-titles">
                        <NavLink to={'/merchant/messages'} activeClassName={'active'}
                                 className="page-title">Messages</NavLink>
                        <NavLink to={'/merchant/land'} activeClassName={'active'} className="page-title">Lands</NavLink>
                        <NavLink to={'/merchant/profile'} activeClassName={'active'}
                                 className="page-title">Profile</NavLink>
                    </div>
                    <button
                        className="btn primary"
                        onClick={() => setIsAddLandModalOpen(true)}
                    >
                        Add Land
                    </button>
                </div>
                <div className="merchant-content">
                    <Switch>
                        <Route path={"/merchant/land"} component={Lands} exact={true}/>
                        <Route path={"/merchant/messages"} component={Messages} exact={true}/>
                        <Route path={"/merchant/profile"} component={Profile} exact={true}/>
                        <Route path={""} component={() => <Redirect to={'merchant/messages'}/>} exact={true}/>
                    </Switch>
                </div>
            </section>
            {isAddLandModalOpen && (
                <AddLand modalDisappear={() => setIsAddLandModalOpen(false)}/>
            )}
        </>
    );
};
const mapStateToProps = state => {
    return {
        displayAddLand: state.landReducer.displayAddLand,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        toggleLandModal: () => dispatch(toggleAddLand(true)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Merchant);
