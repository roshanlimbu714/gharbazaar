import React, {useState} from 'react'
import {connect} from 'react-redux';
import * as actions from '../../../store/actions'

const AddMessage = props => {
    const [message, setMessage] = useState({});

    const inputHandler = e => {
        setMessage({...message, [e.target.name]: e.target.value});
    };

    const addMessage = async (e) => {
        e.preventDefault();
        let _listing = props.landId;
        let _merchant = props.merchantId;

        await props.addMessage({...message, _listing, _merchant});
        props.modalDisappear();
    };
    return (
        <form className="add-message-modal" onSubmit={addMessage}>
            <div className="title">Leave query</div>
            <div className="input-box">
                <label>Name</label>
                <input type="text" name={'name'} onKeyDown={inputHandler} placeholder={'Enter name'}/>
            </div>
            <div className="input-box">
                <label>Address</label>
                <input type="text" name={'address'} onKeyDown={inputHandler} placeholder={'Enter address'}/>
            </div>
            <div className="input-box">
                <label>Phone</label>
                <input type="text" name={'phone'} onKeyDown={inputHandler} placeholder={'Enter phone'}/>
            </div>
            <div className="input-box">
                <label>Message</label>
                <input type="text" name={'message'} onKeyDown={inputHandler} placeholder={'Enter message'}/>
            </div>
            <div className="button-area d-flex justify-end">
                <button className="btn primary">Submit</button>
            </div>
        </form>
    )
};


const mapDispatchToProps = dispatch => {
    return {
        addMessage: (message) => dispatch(actions.addMessage(message))
    }
}
export default connect(null, mapDispatchToProps)(AddMessage)
