import React, { useEffect, useState } from "react";
import mock from "../../../assets/images/mock.png";
import * as actions from "../../../store/actions/index";
import { connect } from "react-redux";
import Modal from "../../../containers/Modal";
import EditProfile from "./EditProfile";

const Profile = props => {
  const [user, setUser] = useState({});
  const [isEditProfileModal, setIsEditProfileModal] = useState(false);
  useEffect(() => {
    setUser(props.userInfo);
  }, [props.userInfo]);
  return (
    <>
      <div className="profile">
        <div className="land-details d-flex">
          <div className="land-img">
            <img src={mock} alt="" />
          </div>
          <div className="land-info">
            <div className="land-title title">{user.name}</div>
            <br />

            <div className="land-detail">
              <label>UserName: </label>
              <div> {user.username}</div>
            </div>
            <div className="land-detail">
              <label>Address:</label>
              <div>{user.address}</div>
            </div>
            <div className="land-detail">
              <label>Phone:</label>
              <div>{user.phone} </div>
            </div>
            <br />
            <button
              className="btn primary"
              onClick={() => setIsEditProfileModal(true)}
            >
              Edit
            </button>
          </div>
        </div>
      </div>
      {isEditProfileModal && (
        <Modal modalDisappear={() => setIsEditProfileModal(true)}>
          <EditProfile
            modalDisappear={() => setIsEditProfileModal(false)}
            userInfo={props.userInfo}
          />
        </Modal>
      )}
    </>
  );
};
const mapStateToProps = state => {
  return {
    userInfo: state.authenticationReducer.user,
  };
};
export default connect(mapStateToProps)(Profile);
