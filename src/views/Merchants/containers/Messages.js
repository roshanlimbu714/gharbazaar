import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {deleteLand, getUserMessage} from "../../../store/actions";

const Messages = props => {
    const [userMessages, setUserMessages] = useState([]);
    useEffect(() => {
        props.getMessages();
    }, [props.user]);

    useEffect(() => {
        setUserMessages(props.userMessages);
        console.log(props.userMessages);
    }, [props.userMessages])
    return (
        <div className="table-area">
            <div className="table-titles">
                <div className="table-title flex-5">Name</div>
                <div className="table-title flex-3">Phone</div>
                <div className="table-title flex-3">Address</div>
                <div className="table-title flex-3">Message</div>
                <div className="table-title flex-3">Land Title</div>
            </div>
            <div className="table-content">
                {props.userMessages.map(cust => (
                    <div className="row ">
                        <div className="col flex-5">{cust.name}</div>
                        <div className="col flex-3">{cust.phone}</div>
                        <div className="col flex-3">{cust.address}</div>
                        <div className="col flex-3">{cust.message}</div>
                        <div className="col flex-3">{cust._listing? cust._listing.title:''}</div>
                    </div>
                ))}
            </div>
        </div>
    );
};
const mapStateToProps = state => {
    return {
        user: state.authenticationReducer.user,
        userMessages: state.messagesReducer.userMessages,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        deleteLand: landId => dispatch(deleteLand(landId)),
        getMessages: () => dispatch(getUserMessage())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);
