import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";

const EditLand = props => {
  const [land, setLand] = useState({});
  let editLand = e => {
    e.preventDefault();
  };

  useEffect(() => {
    setLand(props.land);
  }, []);

  let inputHandler = e => {
    setLand({ ...land, [e.target.name]: e.target.value });
  };
  return (
    <section className="land-section flex-centered">
      <div className="background" onClick={props.displayAddLand}></div>
      <form className="card d-flex flex-column" onSubmit={e => editLand(e)}>
        <div className="title">Edit Land</div>
        <div className="input-area flex-one">
          <div className="input-box icon-input">
            <input
              type="text"
              name={"title"}
              value={land.title}
              placeholder={"Enter title"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"description"}
              value={land.description}
              placeholder={"Enter description"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"address"}
              value={land.address}
              placeholder={"Enter address"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"floors"}
              value={land.floors}
              placeholder={"Enter floors"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"rooms"}
              value={land.rooms}
              placeholder={"Enter rooms"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"bathroom"}
              value={land.bathroom}
              placeholder={"Enter bathroom"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box icon-input">
            <input
              type="text"
              name={"road"}
              value={land.road}
              placeholder={"Enter road"}
              onChange={inputHandler}
            />
            <i className="material-icons">face</i>
          </div>
          <div className="input-box">
            <label>Garage</label>
            <input
              type="checkbox"
              name={"garage"}
              value={land.garage}
              placeholder={"Enter garage"}
              onChange={inputHandler}
            />
          </div>
        </div>
        <div className="buttons-area">
          <button
            className={"btn primary"}
            onClick={() => props.editLand({ editinfo: land })}
            onClickCapture={props.modalDisappear}
          >
            Conform
          </button>
          <button className={"btn primary"} onClick={props.modalDisappear}>
            Cancel
          </button>
        </div>
      </form>
    </section>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addLand: land => dispatch(actions.addLand(land)),
    editLand: id => dispatch(actions.editLand(id)),
  };
};

export default connect(null, mapDispatchToProps)(EditLand);
