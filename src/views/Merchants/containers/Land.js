import React, { useEffect, useState } from "react";
import mock from "../../../assets/images/mock.png";
import { useParams } from "react-router";
import Modal from "../../../containers/Modal";
import * as actions from "../../../store/actions/index";
import { connect } from "react-redux";
import AddMessage from "./AddMessage";

const Land = props => {
  const { id } = useParams();
  const [land, setLand] = useState({});
  const [addMessageModal, setAddMessageModal] = useState(false);
  useEffect(() => {
    props.getLand(id);
  }, [id]);

  useEffect(() => {
    setLand(props.land);
    console.log(props.land);
  }, [props.land]);
  return (
    <>
      <div className="land-details d-flex">
        <div className="land-img">
          <img src={mock} alt="" />
        </div>
        <div className="land-info">
          <div className="land-title title">{land.title}</div>
          <div className="land-description">{land.description}</div>
          <div className="land-detail">
            <label>Road:</label>
            <div>{land.road}</div>
          </div>
          <div className="land-detail">
            <label>Floors:</label>
            <div>{land.floors}</div>
          </div>
          <div className="land-detail">
            <label>Bathroom:</label>
            <div>{land.bathroom}</div>
          </div>
          <div className="land-detail">
            <label>Garage:</label>
            <div>{land.garage ? "Yes" : "No"}</div>
          </div>
          <div className="land-detail bold title-case">
            <label>Sold by:</label>
            <div>{land._merchant && land._merchant.name}</div>
          </div>
          <button
            className="btn primary"
            onClick={() => setAddMessageModal(true)}
          >
            Leave Message
          </button>
        </div>
      </div>
      {addMessageModal && (
        <Modal modalDisappear={() => setAddMessageModal(false)}>
          {console.log(land)}
          <AddMessage
            modalDisappear={() => setAddMessageModal(false)}
            landId={land._id}
            merchantId={land._merchant._id}
          />
        </Modal>
      )}
    </>
  );
};

const mapStateToProps = state => {
  return {
    land: state.landReducer.land,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getLand: id => dispatch(actions.getSingleLand(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Land);
