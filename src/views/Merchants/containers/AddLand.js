import React, {useState} from "react";
import {connect} from "react-redux";
import * as actions from "../../../store/actions/index";

const AddLand = props => {
    const [land, setLand] = useState({});
    let addLand = e => {
        e.preventDefault();
        props.addLand(land);
    };
    const inputHandler = e => {
        setLand({...land, [e.target.name]: e.target.value})
    };
    return (

        <section className="land-section flex-centered">
            <div className="background" onClick={props.displayAddLand}></div>
            <form className="card d-flex flex-column" onSubmit={e => addLand(e)}>
                <div className="title">Add Land</div>
                <div className="input-area flex-one">
                    <div className="input-box icon-input">
                        <label>title</label>
                        <input
                            type="text"
                            name={'title'}
                            placeholder={"Enter title"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">dashboard</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>description</label>
                        <input
                            type="text"
                            name={'description'}
                            placeholder={"Enter description"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">book</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>price</label>
                        <input
                            type="text"
                            name={'price'}
                            placeholder={"Enter price"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">attach_money</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>address</label>
                        <input
                            type="text"
                            name={'address'}
                            placeholder={"Enter address"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">room</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>floors</label>
                        <input
                            type="text"
                            name={'floors'}
                            placeholder={"Enter floors"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">reorder</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>rooms</label>
                        <input
                            type="text"
                            name={'rooms'}
                            placeholder={"Enter rooms"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">border_clear</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>bathroom</label>
                        <input
                            type="text"
                            name={'bathroom'}
                            placeholder={"Enter bathroom"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">bathtub</i>
                    </div>
                    <div className="input-box icon-input">
                        <label>road</label>
                        <input
                            type="text"
                            name={'road'}
                            placeholder={"Enter road"}
                            onChange={inputHandler}
                        />
                        <i className="material-icons">directions_bus</i>
                    </div>
                    <div className="input-box">
                        <label className="container">Garage
                            <input type="checkbox" name={'garage'} value={true} onChange={inputHandler}/>
                            <span className="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div className="buttons-area d-flex justify-end">
                    <button className={"btn primary"}>Add Land</button>
                    <button
                        className={"btn dark"}
                        onClick={props.modalDisappear}
                    >
                        Cancel
                    </button>
                </div>
            </form>
        </section>
    );
};


const mapDispatchToProps = dispatch => {
    return {
        addLand: land => dispatch(actions.addLand(land)),
    };
};

export default connect(null, mapDispatchToProps)(AddLand);
