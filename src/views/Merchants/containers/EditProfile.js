import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";

const EditProfile = props => {
  const [userInfo, setUserInfo] = useState(props.userInfo);
  const [oldUser, setOldUser] = useState([]);
  useEffect(() => {
    setOldUser(props.userInfo);
  }, [[props.userInfo]]);
  let inputHandler = e => {
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
  };

  const editProfile = () => {
    console.log(userInfo);
  };
  return (
    <>
      <form onSubmit={editProfile}>
        <div className="input-box">
          <label>Name</label>
          <input
            type="text"
            placeholder={"Enter name"}
            name="name"
            onKeyUp={inputHandler}
          />
        </div>
        <div className="input-box">
          <label>Username</label>
          <input
            type="text"
            placeholder={"Enter username"}
            name="username"
            onKeyUp={inputHandler}
          />
        </div>
        <div className="input-box">
          <label>Password</label>
          <input
            type="text"
            placeholder={"Enter password"}
            name="password"
            onKeyUp={inputHandler}
          />
        </div>
        <div className="input-box">
          <label>Address</label>
          <input
            type="text"
            placeholder={"Enter address"}
            name="address"
            onKeyUp={inputHandler}
          />
        </div>
        <div className="input-box">
          <label>Phone</label>
          <input
            type="tel"
            placeholder={"Enter phone"}
            name="phone"
            onKeyUp={inputHandler}
          />
        </div>
      </form>
      <button
        className={"btn primary"}
        onClick={() => props.editUsers(userInfo)}
        onClickCapture={props.modalDisappear}
      >
        Edit Profile
      </button>
      <button className={"btn primary"} onClick={props.modalDisappear}>
        Cancel
      </button>
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    editUsers: newuseInfo => dispatch(actions.authEdit(newuseInfo)),
  };
};
const mapStateToProps = state => {
  return {
    userInfo: state.authenticationReducer.user,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
