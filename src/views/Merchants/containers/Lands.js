import React, { useEffect, useState } from "react";
import mockLand from "../../../assets/images/mock.png";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import { useHistory } from "react-router";
import EditLand from "./EditLand";
import Modal from "../../../containers/Modal";

const Lands = props => {
  const [userResults, setUserResults] = useState([]);
  const [land, setLand] = useState({});
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [landId, setLandId] = useState("");
  let history = useHistory();

  useEffect(() => {
    props.getUserLands();
  }, []);

  useEffect(() => {
    setUserResults(props.userResults);
  }, [props.userResults]);
  const deleteLand = async () => {
    await props.deleteLand(landId);
    setIsDeleteModalOpen(false);
  };
  const openEditModal = land => {
    setLand(land);
    setIsEditModalOpen(true);
  };

  return (
    <>
      <div className="title">User Lands</div>
      <div className="land-list d-flex flex-wrap">
        {userResults &&
          userResults.map(land => (
            <article>
              <div className="article-img">
                <img src={mockLand} alt={"mock"} />
              </div>
              <div
                className="article-title"
                onClick={() => history.push("/land/" + land._id)}
              >
                {land.title}
              </div>
              <div className="article-description">
                <i className="material-icons">room</i>
                {land.address}
              </div>
              <div className="article-description d-flex align-center primary-text">
                <b className="">Rs. </b>
                <div className="flex-one">{land.price}</div>
              </div>
              <div className="button-area d-flex justify-evenly">
                <button
                  className="btn primary flex-1"
                  onClick={() => openEditModal(land)}
                >
                  Edit
                </button>
                <button
                  className="btn dark flex-1"
                  onClick={() => {
                    setIsDeleteModalOpen(true);
                    setLandId(land._id);
                  }}
                >
                  Delete
                </button>
              </div>
            </article>
          ))}
      </div>
      {isEditModalOpen && (
        <Modal modalDisappear={() => setIsEditModalOpen(false)}>
          <EditLand
            modalDisappear={() => setIsEditModalOpen(false)}
            land={land}
          />
        </Modal>
      )}
      {isDeleteModalOpen && (
        <Modal modalDisappear={() => setIsEditModalOpen(false)}>
          <div className="title text-center">
            Are you sure you want to delete?
          </div>
          <div className="button-area d-flex space-around">
            <button className="btn primary" onClick={deleteLand}>
              Yes
            </button>
            <button
              className="btn dark"
              onClick={() => setIsDeleteModalOpen(false)}
            >
              No
            </button>
          </div>
        </Modal>
      )}
    </>
  );
};

const mapStateToProps = state => {
  return {
    userResults: state.landReducer.userLands,
    id: state.authenticationReducer.id,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserLands: () => dispatch(actions.getUserLands()),
    deleteLand: id => dispatch(actions.deleteLand(id)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Lands);
