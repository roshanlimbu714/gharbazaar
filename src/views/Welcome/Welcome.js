import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import mockLand from "../../assets/images/mock.png";

const Welcome = props => {
  const history = useHistory();
  const [search, setSearch] = useState("");
  return (
    <>
      <section className="home">
        <div className="text-area d-flex flex-column justify-center">
          <div className="title">Find your dreamhouse with us</div>
          <div className="input-area d-flex align-center">
            <input
              type="text"
              name="SearchTitle"
              onChange={e => setSearch(e.target.value)}
              className="flex-one"
              placeholder="Search Everything"
            />
            <i
              className="material-icons"
              onClick={() => {
                history.push("/search/" + search);
              }}
            >
              search
            </i>
          </div>
        </div>
        <div className="image-area"></div>
      </section>
      <section>
        <header className="sticky-top">Popular Today</header>
        <div className="articles">
          {props.lands.map(land => (
            <article>
              <div className="article-img">
                <img
                  src={mockLand}
                  alt={"Mock"}
                  onClick={() => history.push("/land/" + land._id)}
                />
              </div>
              <div className="article-title">{land.title}</div>
              <div className="article-description">
                <i className="material-icons">room</i>
                {land.address}
              </div>
              <div className="article-description d-flex align-center primary-text">
                <b className="">Rs. </b>
                <div className="flex-one">Price</div>
              </div>
              <div className="article-subtitle d-flex align-center">
                <i className="material-icons">face</i>Ram Bilas
              </div>
            </article>
          ))}
        </div>
      </section>
      <section>
        <header className="sticky-top">Latest Offers</header>
        <div className="articles">
          {props.lands.slice(-5).map(latest => (
            <article>
              <div
                className="article-img"
                key={latest._id}
                onClick={() => history.push("/land/" + latest._id)}
              >
                <img src={mockLand} alt={"Mock"} />
              </div>
              <div className="article-title">{latest.title}</div>
              <div className="article-description">
                <i className="material-icons">room</i>
                {latest.address}
              </div>
              <div className="article-description d-flex align-center primary-text">
                <b className="">Rs. </b>
                <div className="flex-one">{latest.price}</div>
              </div>
            </article>
          ))}
        </div>
      </section>
    </>
  );
};
const mapStateToProps = state => {
  return {
    lands: state.landReducer.lands,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
