import React from "react";
import * as action from "../store/actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

const Nav = props => (
    <nav>
        <div className="logo">
            <Link to="">Ghar<span className="primary-text">Bazaar</span></Link>
        </div>
        <div className="nav-items d-flex justify-end">
            {/*<div className="nav-item"></div>*/}
            {/*<div className="nav-item">home</div>*/}
            {/*<div className="nav-item">home</div>*/}
            <div className="nav-item bold user justify-self-end">
                <Link to={'/merchant'}>{props.user.username}
                </Link>
                <div className="user-drop-down">
                    <div className="user-drop-down-item" onClick={props.logout}>
                        Logout
                    </div>
                </div>
            </div>
            {!props.isLoggedIn && (
                <>
                    <button className="btn primary" onClick={props.displayLogin}>
                        Login
                    </button>
                    <Link to="/register" className={'btn dark'}>Register</Link>
                </>
            )}
        </div>
    </nav>
);
const mapDispatchToProps = dispatch => {
    return {
        displayLogin: () => dispatch(action.displayLoginModal(true)),
        logout: () => dispatch(action.logout())
    };
};

const mapStateToProps = state => {
    return {
        isLoggedIn: state.authenticationReducer.isLoggedIn,
        user: state.authenticationReducer.user,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Nav);
