import React from 'react'

const Modal = props => {
    return (
        <div className="gn-modal flex-centered">
            <div className="modal-background" onClick={props.modalDisappear}></div>
            <div className="modal-card d-flex flex-column card">
                <div className="modal-title">{props.title}</div>
                <div className="modal-content flex-1">
                    {props.children}
                </div>

            </div>
        </div>
    );
};

export default Modal;
