import React from 'react';

const Footer = () => (
    <footer className="d-flex align-center flex-column justify-between">
        <div className="breaker"></div>
        <button className="btn primary d-flex align-center d-animate-none"><i
            className="material-icons">favorite</i><span>Subscribe</span></button>
        <div className="footer-end accent-bg w-100">
            <div>Terms of Service</div>
            <div>Privdivcy Policy</div>
            <div>Report Abuse</div>
        </div>
    </footer>
);

export default Footer;
