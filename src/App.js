import React from "react";
// import logo from './logo.svg';
import "./App.scss";
import Layout from "./hoc/Layout";

function App() {
  return <Layout />;
}

export default App;
