//authentication data

export const LOGOUT = "LOGOUT";
export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const LOGIN_MODAL_TOGGLE = "LOGIN_MODAL_TOGGLE";
export const REGISTER_USER = "REGISTER_USER";

export const GET_ALL_MESSAGES = "GET_ALL_MESSAGES";
export const GET_MESSAGE = "GET_ALL_MESSAGE";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const GET_USER_MESSAGES = "GET_USER_MESSAGES";

export const TOGGLE_LAND_MODAL = "TOGGLE_LAND_MODAL";
export const GET_ALL_LANDS = "GET_ALL_LANDS";
export const GET_LAND = "GET_LAND";
export const ADD_LAND = "ADD_LAND";
export const GET_SEARCHED_LAND = "GET_SEARCHED_LAND";
export const GET_USER_LANDS = "GET_USER_LANDS";
export const GET_SINGLE_LAND = "GET_SINGLE_LAND";
export const EDIT_LAND = "EDIT_LAND";
