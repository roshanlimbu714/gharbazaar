import * as actionTypes from "./actions";
import axios from "axios";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const registerUserSuccess = user => {
  return {
    type: actionTypes.REGISTER_USER,
    user: user,
  };
};

export const authSuccess = authData => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    authData: authData,
  };
};
export const logOutSuccess = () => {
  return {
    type: actionTypes.LOGOUT,
  };
};
export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error,
  };
};
export const authEdit = newuseinfo => {
  return dispatch => {
    axios.put("http://localhost:3030/api/v1/merchant", newuseinfo).then(res => {
      console.log(res);
      logout();
      // registerUserSuccess(res.data.data);
    });
  };
};
export const toggleLoginModal = mode => {
  return {
    type: actionTypes.LOGIN_MODAL_TOGGLE,
    mode: mode,
  };
};

export const displayLoginModal = mode => {
  return dispatch => {
    dispatch(toggleLoginModal(mode));
  };
};

export const authenticate = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    axios
      .post("http://localhost:3030/api/v1/login", { username, password })
      .then(
        res => {
          localStorage.setItem("token", res.data.token);
          dispatch(authSuccess(res.data.result));
          dispatch(displayLoginModal(false));
        },
        err => {},
      );
  };
};

export const registerUser = user => {
  return dispatch => {
    dispatch(authStart());
    console.log(user);
    axios
      .post("http://localhost:3030/api/v1/merchant", user)
      .then(res => {
        localStorage.setItem("token", res.data.token);
        dispatch(authSuccess({ user: {} }));
      })
      .catch(err => console.log(err));
  };
};

export const checkIfAuthenticated = () => {
  let user = localStorage.getItem("token");
  return dispatch => {
    axios
      .post("http://localhost:3030/api/v1/reauthenticate", { token: user })
      .then(res => {
        // localStorage.setItem('token', res.data.token);
        dispatch(displayLoginModal(false));
        dispatch(authSuccess(res.data["user"]));
      })
      .catch(err => console.log(err));
  };
};

export const logout = () => {
  return dispatch => {
    localStorage.removeItem("token");
    dispatch(logOutSuccess());
  };
};
