import axios from "axios";
let SearchAction = searchTitle => {
  return dispatch => {
    console.log("From search");
    dispatch({ type: "SearchList_Fetch" });
    axios
      .post("http://localhost:3030/api/v1/search/", searchTitle)
      .then(res => {
        console.log(res);
        dispatch({ type: "Search_Title_Received", payload: res });
      })
      .catch(err => {
        dispatch({ type: "Search_Title_Error", payload: err });
        console.error(err);
      });
  };
};
export default SearchAction;
