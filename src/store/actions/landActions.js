import * as actionTypes from "./actions";
import axios from "axios";
import { store } from "../store";

export const toggleAddLand = mode => {
  return {
    type: actionTypes.TOGGLE_LAND_MODAL,
    mode: mode,
  };
};
export const getLands = lands => {
  return {
    type: actionTypes.GET_ALL_LANDS,
    lands: lands,
  };
};
export const searchResultsSuccess = lands => {
  return {
    type: actionTypes.GET_SEARCHED_LAND,
    lands: lands,
  };
};
export const getUserLandsSuccess = lands => {
  return {
    type: actionTypes.GET_USER_LANDS,
    lands: lands,
  };
};

export const addLandSuccess = () => {
  loadLands();
  return {
    type: actionTypes.ADD_LAND,
  };
};

export const loadLands = () => {
  return dispatch => {
    axios.get("http://localhost:3030/api/v1/land").then(res => {
      dispatch(getLands(res.data.data.lands));
    });
  };
};

export const addLand = land => {
  return dispatch => {
    let _merchant = store.getState().authenticationReducer.user._id;
    land = { ...land, _merchant };
    axios
      .post("http://localhost:3030/api/v1/land", land)
      .then(res => dispatch(addLandSuccess()));
  };
};

export const deleteLand = id => {
  return dispatch => {
    axios.delete("http://localhost:3030/api/v1/land/" + id).then(res => {
      getUserLands();
      dispatch(getUserLands());
    });
  };
};

export const editLand = id => {
  return dispatch => {
    axios.put("http://localhost:3030/api/v1/land", id).then(res => {
      getUserLands();
      dispatch(getUserLands());
    });
  };
};

export const searchLand = term => {
  return dispatch => {
    axios
      .post("http://localhost:3030/api/v1/searchLand", { term })
      .then(res => {
        dispatch(searchResultsSuccess(res.data.data.lands));
      });
  };
};

export const getUserLands = () => {
  return dispatch => {
    let id = store.getState().authenticationReducer.user._id;
    console.log(id);
    axios
      .post("http://localhost:3030/api/v1/userLands", { id: id })
      .then(res => {
        dispatch(getUserLandsSuccess(res.data.data.lands));
      });
  };
};
export const getSingleLandSuccess = land => {
  return {
    type: actionTypes.GET_SINGLE_LAND,
    land: land,
  };
};
export const getSingleLand = id => {
  return dispatch => {
    axios.get("http://localhost:3030/api/v1/land/" + id).then(res => {
      dispatch(getSingleLandSuccess(res.data.data));
    });
  };
};
