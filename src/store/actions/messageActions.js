import axios from 'axios';
import * as actionTypes from './actions';
import {store} from "../store";

export const addMessageSuccess = () => {
    return {
        type: actionTypes.ADD_MESSAGE,
    }
};

export const getUserMessagesSuccess = messages => {
    return {
        type: actionTypes.GET_USER_MESSAGES,
        userMessages: messages
    }
}
export const getMessages = messages => {
    return {
        type: actionTypes.GET_MESSAGE,
        messages: messages
    }
};

export const addMessage = (msg) => {
    let message = {...msg};
    return dispatch => {
        axios.post("http://localhost:3030/api/v1/message/", message).then(res => {
            console.log(res);
        })
    }
};

export const getAllMessages = () => {

};

export const getUserMessage = () => {
    return dispatch => {
        // let id = store.getState().authenticationReducer.user._id;
        let merchantId = store.getState().authenticationReducer.user._id;
        console.log(merchantId);
        axios.get("http://localhost:3030/api/v1/merchantMessages/" + merchantId).then(res => {
            dispatch(getUserMessagesSuccess(res.data.data))
        });
    }
}
