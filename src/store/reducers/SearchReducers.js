let initialState = {
  fetch: false,
  fetched: false,
  error: false,
};
let SearchReducers = (state = initialState, aciton) => {
  switch (aciton.type) {
    case "SearchList_Fetch":
      return { ...state, fetch: true };
    case "Search_Title_Received":
      return { ...state, fetched: true, SearcedTitle: aciton.payload };
    case "Search_Title_Error":
      return { ...state, error: true, SearchError: aciton.payload };
    default:
      return state;
  }
};
export default SearchReducers;
