import * as actionTypes from '../actions/actions'

const initialState = {
    messages: [],
    message: {},
    userMessages: []
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };
        case actionTypes.GET_MESSAGE:
            return {
                ...state,
                message: action.message
            };
        case actionTypes.ADD_MESSAGE:
            return {
                ...state
            };
        case actionTypes.GET_USER_MESSAGES:
            return {
                ...state,
                userMessages: action.userMessages
            };
        default:
            return state;

    }
};
export default messagesReducer;
