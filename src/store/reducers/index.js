import { combineReducers } from "redux";
import authenticationReducer from "./authenticationReducer";
import messagesReducer from "./messagesReducer";
import landReducer from "./landReducer";
import SearchReducers from "./SearchReducers";
export default combineReducers({
  authenticationReducer,
  messagesReducer,
  landReducer,
  SearchReducers,
});
