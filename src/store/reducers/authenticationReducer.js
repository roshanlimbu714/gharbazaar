import * as actionTypes from '../actions/actions.js';

const initialState = {
    id:'',
    user: {},
    isLoggedIn: false,
    loginAppear: false,
};

const authenticationReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_SUCCESS:
            let user = action.authData;
            return {
                ...state,
                isLoggedIn: true,
                user: user,
                id: user._id
            };
        case actionTypes.LOGOUT:
            console.log('Logged out state');
            return {
                ...state,
                isLoggedIn: false,
                user:{}
            };
        case actionTypes.LOGIN_MODAL_TOGGLE:
            return {
                ...state,
                loginAppear: action.mode

            };
        case actionTypes.REGISTER_USER:
            return {
                ...state,
                user: action.user,
                isLoggedIn: true
            };


        default:
            return state;
    }
};

export default authenticationReducer;
