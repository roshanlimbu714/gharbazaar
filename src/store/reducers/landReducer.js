import * as actionTypes from '../actions/actions';

const initialState = {
    displayAddLand: false,
    lands: [],
    searchResults: [],
    land: {},
    userLands: []
};

const landReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_LAND_MODAL:
            console.log(action.mode);
            return {
                ...state,
                displayAddLand: action.mode
            };
        case actionTypes.GET_ALL_LANDS:
            return {
                ...state,
                lands: action.lands
            };
        case actionTypes.ADD_LAND:
            return {
                state
            };
        case actionTypes.GET_SEARCHED_LAND:
            return {
                ...state,
                searchResults: action.lands,
            };
        case actionTypes.GET_USER_LANDS:
            return {
                ...state,
                userLands: action.lands,
            };
        case actionTypes.GET_SINGLE_LAND:
            return {
                ...state,
                land: action.land
            };
        default:
            return state;
    }
}
export default landReducer;
