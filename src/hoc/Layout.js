import React, {useEffect} from "react";
import Nav from "../containers/Nav";
import Footer from "../containers/Footer";
import Welcome from "../views/Welcome/Welcome";
import {Redirect, Route, Switch} from "react-router";
import {connect} from "react-redux";
import Login from "../views/Authentication/Login";
import Register from "../views/Authentication/Register";
import * as actions from "../store/actions/index";
import Merchant from "../views/Merchants/Merchant";
import Search from "../views/Search/Search";
import Land from "../views/Merchants/containers/Land";

const Layout = props => {
    useEffect(() => {
        props.checkIfAuthenticated();
        props.loadLands();
    }, []);

    return (
        <>
            <Nav/>
            <main>
                <Switch>
                    <Route path={"/register"}
                           component={props.isLoggedIn ? () => <Redirect to={'/merchant'}/> : Register} exact={true}/>
                    <Route path={"/merchant"} component={props.isLoggedIn ? Merchant : () => <Redirect to={''}/>}/>
                    <Route path={"/search/:term"} component={Search} exact={true}/>
                    <Route path={"/land/:id"} component={Land} exact={true}/>
                    <Route path={""} component={Welcome} exact={true}/>
                </Switch>
            </main>
            <Footer/>
            {props.loginAppear && <Login/>}
        </>
    );
};
const mapStateToProps = state => {
    return {
        isLoggedIn: state.authenticationReducer.isLoggedIn,
        loginAppear: state.authenticationReducer.loginAppear,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        checkIfAuthenticated: () => dispatch(actions.checkIfAuthenticated()),
        loadLands: () => dispatch(actions.loadLands()),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Layout);
