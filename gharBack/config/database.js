//Set up mongoose connection
const mongoose = require('mongoose');
// const mongoDB = 'mongodb+srv://rosanyonghang:gharbazaar@gharbazaar-wf4pb.mongodb.net/test?retryWrites=true&w=majority';
const mongoDB = "mongodb://rosan:gharbazaar123@ds145474.mlab.com:45474/gharbazaarclg";
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,})
    .then(() => console.log('DB Connected!'))
    .catch(err => {
        console.log(`DB Connection Error: ${err.message}`);
    });
mongoose.Promise = global.Promise;
module.exports = mongoose;
