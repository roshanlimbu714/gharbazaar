module.exports = {
    development: {
        port: process.env.PORT || 3030,
        saltingRounds: 10
    }
}
