const merchant = require("./merchant");
const listings = require("./listings");
const search = require("./search");
const message = require("./messages");
module.exports = router => {
    merchant(router);
    listings(router);
    search(router);
    message(router);
    return router;
};
