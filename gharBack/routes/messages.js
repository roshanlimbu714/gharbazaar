const controller = require("../controllers/message");

module.exports = router => {
    router
        .route("/message")
        .post(controller.addMessage)
        .get(controller.getAll);

    router.route("/merchantMessages/:id")
        .get(controller.getMerchantMessages);
};
