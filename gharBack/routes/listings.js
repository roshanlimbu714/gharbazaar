const controller = require("../controllers/listings");

module.exports = router => {
  router.route("/land/:id").get(controller.getSingleLand);
  router
    .route("/land")
    .post(controller.addLand)
    .get(controller.getAll)
    .put(controller.editLand);
  router.route("/LatestLand").get(controller.getAll);

  router.route("/land/:id").delete(controller.deleteLand);

  router.route("/searchLand").post(controller.searchLand);

  router.route("/userLands").post(controller.getUserLands);
};
