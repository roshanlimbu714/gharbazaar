const controller = require("../controllers/search");

module.exports = router => {
  console.log("Search Back access");
  router
    .route("/search")
    .get(controller.getLand)
    .post(controller.postLand);
};
