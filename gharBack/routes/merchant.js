const controller = require("../controllers/merchant");

module.exports = router => {
  router
    .route("/merchant")
    .post(controller.register)
    .get(controller.getAll)
    .put(controller.editProfile);
  router.route("/login").post(controller.login);
  router.route("/reauthenticate").post(controller.reauthenticate);
};
