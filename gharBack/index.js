const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('./config/database'); //database configuration
const app = express();
const cors = require('cors');

const router = express.Router();
const routes = require('./routes/index.js');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

// app.set('secretKey', 'nodeRestApi'); // jwt secret token
// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

// app.use(logger('dev'));
app.get('/', function (req, res) {
    res.json({"tutorial": "Build REST API with node.js"});
});

app.use('/api/v1', routes(router));

app.post('/test', (req, res) => {
    console.log(req.body);
    console.log('dasdasd')
});
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    if (err.status === 404)
        res.status(404).json({message: "Not found"});
    else
        res.status(500).json({message: "Something looks wrong :( !!!"});
});

app.listen(3030, function () {
    console.log('Node server listening on port 3030');
});

