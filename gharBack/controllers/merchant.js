const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const mongoose = require("mongoose");
const Merchant = require("../models/merchant");

const connUri =
  "mongodb+srv://rosanyonghang:gharbazaar@gharbazaar-wf4pb.mongodb.net/test?retryWrites=true&w=majority";

module.exports = {
  register: (req, res) => {
    let result = {};
    let status = 201;
    const { name, username, password, address, phone, isActive } = req.body;
    const merchant = new Merchant({
      name,
      username,
      password,
      address,
      phone,
      isActive,
    }); // document = instance of a model
    merchant.save((err, merchant) => {
      if (!err) {
        result.status = status;
        result.result = merchant;
      } else {
        status = 500;
        result.status = status;
        result.error = err;
      }
      res.status(status).send(result);
    });
  },
  getAll: (req, res) => {
    let merchantsList = [];
    let result = {};
    let status = 201;

    // TODO: We can hash the password here before we insert instead of in the model
    Merchant.find({}, (err, merchants, next) => {
      if (err) {
        next(err);
      } else {
        merchantsList = [...merchants];
        res.json({
          status: "success",
          message: "Movies list found!!!",
          data: { movies: merchantsList },
        });
      }
    });
  },
  editProfile: (req, res) => {
    let data = req.body;
    bcrypt.hash(data.password, 10, function(err, hash) {
      if (err) {
        console.log("Error hashing password for merchant", data.name);
        next(err);
      } else {
        let newinfo = {
          $set: {
            name: data.name,
            username: data.username,
            password: hash,
            address: data.address,
            phone: data.phone,
          },
        };

        console.log(req.body);
        Merchant.updateOne({ _id: data._id }, newinfo, (err, result) => {
          if (!err) {
            res.json({
              status: "Success",
              message: "Edit success",
            });
          } else {
            console.log(err);
            res.json({
              status: "Failed",
              message: "Edit Failed",
              data: err,
            });
          }
        });
      }
    });
  },
  login: (req, res) => {
    const { username, password } = req.body;
    // console.log(username, password)
    let result = {};
    let status = 200;
    Merchant.findOne({ username }, (err, user) => {
      if (!err && user) {
        // We could compare passwords in our model instead of below as well
        // bcrypt.hash(merchant.password, 10)
        bcrypt
          .compare(password, user.password)
          .then(match => {
            // console.log(match);
            if (match) {
              status = 200;
              // Create a token
              const payload = { user: user.name, password: user.password };
              const options = { expiresIn: "2d", issuer: "Ghar Bazaar" };
              const secret = "gharbazaar";
              const token = jwt.sign(payload, secret, options);

              // console.log('TOKEN', token);
              result.token = token;
              result.status = status;
              result.result = user;
            } else {
              status = 401;
              result.status = status;
              result.error = `Authentication error`;
            }
            res.status(status).send(result);
          })
          .catch(err => {
            status = 500;
            result.status = status;
            result.error = err;
            res.status(status).send(result);
          });
      } else {
        status = 404;
        result.status = status;
        result.error = err;
        res.status(status).send("auth error");
      }
    });
  },
  reauthenticate: (req, res) => {
    jwt.verify(req.body.token, "gharbazaar", function(err, decoded) {
      Merchant.findOne({ username: decoded.user }, (err, user) => {
        res.json({ status: "success", message: "User log in details", user });
      });
    });
  },
};
