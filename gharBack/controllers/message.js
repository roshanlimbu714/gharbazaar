const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');
const Merchant = require('../models/merchant');
const Listing = require('../models/listings');
const Message = require('../models/messages');


module.exports = {
    addMessage: async (req, res) => {
        let result = {};
        let status = 201;
        let listing = {};
        let merchant = {};
        const {name, phone, address, message} = req.body;
        // await Listing.findOne({_id: req.body['landId']}, (err, res) => {
        //     listing = res;
        // });
        // await Merchant.findOne({_id: req.body['merchantId']}, (err, res) => {
        //     merchant = res;
        // });

        let newMessage = new Message(req.body);
        newMessage.save((err, message) => {
            if (err) {
                console.log(err);
            } else {
                res.json({status: "success", message: "Message add!!!", data: {movies: message}});
            }
        })
    },
    getAll: (req, res) => {
        let messagesList = [];
        let result = {};
        let status = 201;

        // TODO: We can hash the password here before we insert instead of in the model
        Message.find({}, (err, messages, next) => {
            if (err) {
                next(err);
            } else {
                messagesList = [...messages];
                // res.json({status: "success", message: "messages!!!", data: {movies: messagesList}});
                console.log(messages);
            }

        }).populate('listing');
    },
    getMerchantMessages: (req, res) => {
        Message.find({_merchant: req.params.id}, (err, messages, next) => {
            if (err) {
                console.log(err);
            } else {
                let messagesList = [...messages];
                res.json({status: "success", message: "messages!!!", data: messagesList});
                console.log(messages);
            }

        }).populate('_listing');
    }
};


