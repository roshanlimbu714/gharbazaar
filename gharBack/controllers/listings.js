// const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');

const Listing = require("../models/listings");
const Merchant = require("../models/merchant");

module.exports = {
  getAll: (req, res) => {
    let landList = [];
    // TODO: We can hash the password here before we insert instead of in the model
    Listing.find({}, (err, lands, next) => {
      if (err) {
        next(err);
      } else {
        landList = [...lands];
        res.json({
          status: "success",
          message: "Lands list found!!!",
          data: { lands: landList },
        });
      }
    });
  },
  getLatestLands: (req, res) => {
    Listing.find(
      {},
      { limit: 4, sort: { published_date: -1 } },
      (err, lands) => {
        if (err) {
          console.log(err);
        } else {
          res.json({
            status: "success",
            message: "Lands list found!!!",
            data: { lands: lands },
          });
        }
      },
    );
  },
  getPopularLands: (req, res) => {
    Listing.find({}, { limit: 4 }, (err, lands) => {
      if (err) {
        console.log(err);
      } else {
        res.json({
          status: "success",
          message: "Lands list found!!!",
          data: { lands: lands },
        });
      }
    });
  },
  addLand: (req, res) => {
    let result = {};
    let status = 201;
    console.log(req.body.garage);
    const listing = new Listing(req.body);
    listing.save((err, listing) => {
      // console.log(err, listing);
      if (!err) {
        result.status = status;
        result.result = listing;
      } else {
        status = 500;
        result.status = status;
        result.error = err;
      }
      res.status(status).send(result);
    });
    // document = instance of a model
  },
  deleteLand: (req, res) => {
    Listing.deleteOne({ _id: req.params.id }, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.json({
          status: "success",
          message: "Message deleted Successfully",
          data: result,
        });
      }
    });
  },
  editLand: (req, res) => {
    data = req.body;
    let newinfo = {
      $set: {
        title: data.editinfo.title,
        description: data.editinfo.description,
        address: data.editinfo.address,
        floors: data.editinfo.floors,
        rooms: data.editinfo.rooms,
        bathroom: data.editinfo.bathroom,
        road: data.editinfo.road,
        garage: data.editinfo.garage,
      },
    };
    Listing.updateOne({ _id: data.editinfo._id }, newinfo, (err, result) => {
      if (!err) {
        res.json({
          status: "Success",
          message: "Edit success",
        });
      } else {
        console.errro(err);
      }
    });
    console.log("The eid in data base ");
  },
  searchLand: (req, res) => {
    const term = req.body.term;
    console.log(term);
    let landList = [];

    Listing.find(
      { title: { $regex: term, $options: "i" } },
      (err, lands, next) => {
        if (err) {
          next(err);
        } else {
          landList = [...lands];
          console.log(lands);
          res.json({
            status: "success",
            message: "Lands list found!!!",
            data: { lands: landList },
          });
        }
      },
    );
  },
  getUserLands: (req, res) => {
    const userId = req.body.id;
    console.log(req.body);
    let landList = [];

    Listing.find({ _merchant: userId }, (err, lands, next) => {
      if (err) {
        next(err);
      } else {
        landList = [...lands];
        console.log(lands);
        res.json({
          status: "success",
          message: "Lands list found!!!",
          data: { lands: landList },
        });
      }
    });
  },
  getSingleLand: (req, res) => {
    let id = req.params["id"];
    let result = {};
    Listing.findOne({ _id: id }, (err, land, next) => {
      if (err) {
        console.log(err);
      } else {
        const resultLand = { ...land };
        res.json({
          status: "success",
          message: "Land Found !!",
          data: resultLand._doc,
        });
      }
    }).populate("_merchant");
  },
};
