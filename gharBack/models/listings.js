const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const environment = process.env.NODE_ENV;
const stage = require('../config')[environment];

// schema maps to a collection
const Schema = mongoose.Schema;

const listingSchema = new Schema({
    title: {
        type: 'String',
        required: true,
        trim: true,
        unique: true
    },
    description: {
        type: 'String',
        required: true,
        trim: true
    },
    address: {
        type: 'String',
        required: false,
        trim: true
    },
    floors: {
        type: 'String',
        required: true,
        trim: true
    },
    rooms: {
        type: 'Number',
        required: false,
        trim: true
    },
    bathroom: {
        type: 'String',
        required: false
    },
    road: {
        type: 'String',
        required: false
    },
    garage: {
        type: Boolean,
        required: false
    },
    price: {
        type: 'String',
        required: true,
    },
    published_date: {
        type: 'String',
        required: false,
        default: Date.now
    },
    _merchant: {type: mongoose.Schema.Types.ObjectId, ref: 'Merchant', required: true}
});


module.exports = mongoose.model('Listing', listingSchema);
