const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const environment = process.env.NODE_ENV;
const stage = require("../config")[environment];

// schema maps to a collection
const Schema = mongoose.Schema;

const merchantSchema = new Schema({
    name: {
        type: "String",
        required: true,
        trim: true,
    },
    address: {
        type: "String",
        required: false,
        trim: true,
    },
    phone: {
        type: "String",
        required: true,
        trim: true,
    },
    message: {
        type: "String",
        required: true,
        trim: true,
    },
    _listing: {type: mongoose.Schema.Types.ObjectId, ref: "Listing", required: true},
    _merchant: {type: mongoose.Schema.Types.ObjectId, ref: "Merchant", required: true},
});

module.exports = mongoose.model("Message", merchantSchema);
