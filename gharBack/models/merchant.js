const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const environment = process.env.NODE_ENV;
const stage = require('../config')[environment];

// schema maps to a collection
const Schema = mongoose.Schema;

const merchantSchema = new Schema({
    name: {
        type: 'String',
        required: true,
        trim: true,
    },
    username: {
        type: 'String',
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: 'String',
        required: true,
        trim: true
    },
    address:{
        type: 'String',
        required: false,
        trim: true
    },
    phone:{
        type: 'String',
        required: true,
        trim: true
    },
    isActive:{
        type: 'Boolean',
        required: false,
        trim: true,
        default:false
    }
});

merchantSchema.pre('save', function(next) {
    const merchant = this;
    if(!merchant.isModified || !merchant.isNew) { // don't rehash if it's an old user
        next();
    } else {
        bcrypt.hash(merchant.password, 10, function(err, hash) {
            if (err) {
                console.log('Error hashing password for merchant', merchant.name);
                next(err);
            } else {
                merchant.password = hash;
                next();
            }
        });
    }
});


module.exports = mongoose.model('Merchant', merchantSchema);
